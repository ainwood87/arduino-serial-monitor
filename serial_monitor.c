#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define DEFAULT_PORT "/dev/ttyACM0"
#define MAX_READ (100)
int main(int argc, char * argv[])
{
    int rc = 0;
    int write_count = 0;
    const char * serial_port = DEFAULT_PORT;
    if (argc > 1) {
        serial_port = argv[1];
    }

    int fd = open(serial_port, O_RDWR);
    if (fd < 0) {
        perror("unable to open port\n");
        goto fail0;
    }
    fcntl(fd, F_SETFL, FNDELAY); //make reads non-blocking

    char buf[MAX_READ];
    for (;;) {
        rc = read(fd, buf, MAX_READ - 1);
        if (rc < 0) {
            perror("failed to read from port");
            goto fail1;
        } else if (rc > 0) {
            buf[rc] = '\0';
            printf("%s", buf);
        }
        usleep(100000); 
    }
    return 0;
fail1:
    close (fd);
fail0:
    return rc;
}
