all: serial_monitor

serial_monitor.o: serial_monitor.c
	gcc -c serial_monitor.c

serial_monitor: serial_monitor.o
	gcc -o serial_monitor serial_monitor.o

clean:
	rm serial_monitor
	rm *.o
